#! /usr/bin/perl -w
use strict;

use Net::IRC;

sub usage ()
{
	print <<"EOF";
Usage: $0 server my-nick target-channel
EOF
	exit 1;
}

my $server = shift or usage;
my $mynick = shift or usage;
my $targetchannel = shift or usage;

my $irc = new Net::IRC;

my $conn = $irc->newconn(Nick => $mynick, Server => $server)
	or die "$0: can't connect to IRC server";

sub on_connect
{
	my $self = shift;
	$self->who($targetchannel);
}

sub on_whoreply
{
	my ($self, $event) = @_;
	my ($me, $channel, $user, $host,
		$server, $nick, $status, $realname) = $event->args;
	printf "\%-12s  \%s\n", $nick, $realname;
}

sub on_endofwho
{
	my $self = shift;
	$self->quit;
	exit;
}

$conn->add_handler('376', \&on_connect);
$conn->add_handler('352', \&on_whoreply);
$conn->add_handler('315', \&on_endofwho);

$irc->start;
