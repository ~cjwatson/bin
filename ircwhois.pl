#! /usr/bin/perl -w
use strict;

use Net::IRC;

sub usage ()
{
	die <<"EOF";
Usage: $0 server my-nick target-nick [target-nick ...]
EOF
}

my $server = shift or usage;
my $mynick = shift or usage;
my @targetnicks = @ARGV;
usage if $#targetnicks == -1;

my $irc = new Net::IRC;

my $conn = $irc->newconn(Nick => $mynick, Server => $server)
	or die "$0: can't connect to IRC server";

sub on_connect
{
	my $self = shift;
	$self->whois(@targetnicks);
}

sub on_whoisuser
{
	my ($self, $event) = @_;
	my $nick = ($event->args)[1];
	print "$nick is online\n";
}

sub on_endofwhois
{
	my $self = shift;
	$self->quit;
	exit;
}

$conn->add_handler('376', \&on_connect);
$conn->add_handler('311', \&on_whoisuser);
$conn->add_handler('318', \&on_endofwhois);

$irc->start;
