#! /bin/sh
set -e

CONFIG="$1"
if [ -z "$CONFIG" ]; then
	echo "Usage: $0 CONFIG" >&2
	exit 1
fi

NEWLINE='
'

IFS_SAVE="$IFS"
IFS="$NEWLINE"
for line in $(baz cat-config "$1" 2>/dev/null || cat "$1"); do
	IFS="$IFS_SAVE"
	set -- $line
	DIR="$1"
	VERSION="$2"

	echo "Updating $DIR ($VERSION) ..."

	case $VERSION in
		bzr+ssh://*|http://*|sftp://*)
			if [ -d "$DIR" ]; then
				if [ ! -d "$DIR/.bzr" ]; then
					echo "$DIR is not a bzr checkout; cannot update" >&2
					continue
				elif [ -f "$DIR/.bzr/branch/bound" ]; then
					bzr update "$DIR"
				else
					(cd "$DIR" && bzr pull "$VERSION")
				fi
			else
				case $VERSION in
					bzr+ssh://*|sftp://*)
						bzr checkout "$VERSION" "$DIR"
						;;
					*)
						bzr get "$VERSION" "$DIR"
						;;
				esac
			fi
			;;
		git://*|git+*://*)
			if [ -d "$DIR" ]; then
				if [ ! -d "$DIR/.git" ]; then
					echo "$DIR is not a git checkout; cannot update" >&2
					continue
				fi
				(cd "$DIR" && git pull "$VERSION")
			else
				git clone "$VERSION" "$DIR"
			fi
			;;
		*)
			if [ -d "$DIR" ]; then
				if [ ! -d "$DIR/{arch}" ]; then
					echo "$DIR is not an arch checkout; cannot update" >&2
					continue
				fi
				OLDVERSION="$(baz tree-version -d "$DIR")"
				if [ "$VERSION" != "$OLDVERSION" ]; then
					baz switch -d "$DIR" "$VERSION"
				else
					baz update -d "$DIR" "$VERSION"
				fi
			else
				baz get "$VERSION" "$DIR"
			fi
			;;
	esac
done
