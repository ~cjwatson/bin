#! /usr/bin/perl -w
use strict;

use Getopt::Long;
use Net::IRC;

sub usage ()
{
	print <<"EOF";
Usage: $0 [options] command [arguments]
EOF
	print <<'EOF';

Options are as follows, with defaults in brackets:

  -n, --nick=[$IRCNICK]         IRC nickname.
  -s, --server=[$IRCSERVER]     IRC server.

Available commands (with arguments) are:

  who #channel                  List the visible users on a channel.
  whois username ...	        Query information about particular users.
  list                          List channels.

EOF
	exit 1;
}

my %options = (
    nick    => $ENV{IRCNICK},
    server  => $ENV{IRCSERVER},
);

GetOptions(\%options,
    'help|h|?',
    'nick|n=s',
    'server|s=s',
);

usage if $options{help};
unless (defined $options{nick}) {
    print STDERR "No nickname set; use --nick or IRCNICK.\n\n";
    usage;
}
unless (defined $options{server}) {
    print STDERR "No server set; use --server or IRCSERVER.\n\n";
    usage;
}

my $command = shift or usage;
$command = lc $command;

my $irc = new Net::IRC;

# TODO
my $conn = $irc->newconn(Nick => $options{nick}, Server => $options{server})
	or die "$0: can't connect to IRC server";

if ($command eq 'who')
{
    scalar @ARGV == 1 or usage;
    $conn->add_handler('376', sub {
	my $self = shift;
	$self->who($ARGV[1]);
    });
}
elsif ($command eq 'whois')
{
    scalar @ARGV > 0 or usage;
}
elsif ($command eq 'list')
{
    scalar @ARGV == 0 or usage;
    $conn->add_handler('376', sub {
	my $self = shift;
	$self->list('-yes');
    });
}
else
{
    usage;
}

sub on_whoreply
{
	my ($self, $event) = @_;
	my ($me, $channel, $user, $host,
		$server, $nick, $status, $realname) = $event->args;
	printf "\%-12s  \%s\n", $nick, $realname;
}

sub on_endofwho
{
	my $self = shift;
	$self->quit;
	exit;
}

sub on_list
{
    my ($self, $event) = @_;
    print $event->args, "\n";
}

$conn->add_handler('352', \&on_whoreply);
$conn->add_handler('315', \&on_endofwho);
$conn->add_handler('322', \&on_list);

$irc->start;
